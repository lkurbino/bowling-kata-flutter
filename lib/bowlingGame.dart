// ignore_for_file: prefer_initializing_formals, unnecessary_this

import 'package:flutter/material.dart';

class BowlingGame {
  List<IFrame> frames = [];
  List throws = [];
  String finalScore = "";

  void openFrame(int firstThrow, int secondThrow) {
    frames.add(OpenFrame(throws, firstThrow, secondThrow));
  }

  void spare(int firstThrow, int secondThrow) {
    frames.add(SpareFrame(throws, firstThrow, secondThrow));
  }

  void strike() {
    frames.add(StrikeFrame(throws));
  }

  void bonusRoll(int roll) {
    frames.add(BonusRoll(throws, roll));
  }

  int score() {
    int total = 0;
    for (var frame in frames) {
      total += frame.returnScore();
    }
    return total;
  }

  String printedScore() {
    String print = "";
    for (var frame in frames) {
      print += frame.printScore() + " ";
    }
    print = print.substring(0, print.length - 1);
    return print;
  }
}

abstract class IFrame {
  int returnScore();
  String printScore();
  late List throws;
  late int startingThrow;

  IFrame(List throws) {
    this.throws = throws;
    this.startingThrow = throws.length;
  }
}

class OpenFrame extends IFrame {
  OpenFrame(List throws, int firstThrow, int secondThrow) : super(throws) {
    throws.add(firstThrow);
    throws.add(secondThrow);
  }

  @override
  int returnScore() {
    return throws[startingThrow] + throws[startingThrow + 1];
  }

  @override
  String printScore() {
    // TODO: implement printScore
    var firstScore = throws[startingThrow].toString();
    var secondScore = throws[startingThrow + 1].toString();
    if (throws[startingThrow] == 0) {
      firstScore = "-";
    }
    if (throws[startingThrow + 1] == 0) {
      secondScore = "-";
    }
    return firstScore + secondScore;
  }
}

class SpareFrame extends IFrame {
  SpareFrame(List throws, int firstThrow, int secondThrow) : super(throws) {
    throws.add(firstThrow);
    throws.add(secondThrow);
  }

  int _nextBall() {
    return throws[startingThrow + 2];
  }

  @override
  int returnScore() {
    // TODO: implement returnScore
    return 10 + _nextBall();
  }

  @override
  String printScore() {
    // TODO: implement printScore
    var firstScore = throws[startingThrow].toString();
    if (throws[startingThrow] == 0) {
      firstScore = "-";
    }
    return firstScore + "/";
  }
}

class StrikeFrame extends IFrame {
  StrikeFrame(List throws) : super(throws) {
    throws.add(10);
  }

  int _firstFollowingBall() {
    return throws[startingThrow + 1];
  }

  int _secondFollowingBall() {
    return throws[startingThrow + 2];
  }

  @override
  int returnScore() {
    return 10 + _firstFollowingBall() + _secondFollowingBall();
  }

  @override
  String printScore() {
    // TODO: implement printScore
    return "X";
  }
}

class BonusRoll extends IFrame {
  BonusRoll(List throws, int firstThrow) : super(throws) {
    throws.add(firstThrow);
  }

  @override
  int returnScore() {
    // TODO: implement returnScore
    return 0;
  }

  @override
  String printScore() {
    // TODO: implement printScore
    if (throws[startingThrow] == 10) {
      return "X";
    } else if (throws[startingThrow] == 0) {
      return "-";
    }
    return throws[startingThrow].toString();
  }
}
