import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bowling_kata/bowlingGame.dart';
import 'package:bowling_kata/main.dart';

void main() {
  test('Gutter Ball', () {
    BowlingGame game = BowlingGame();

    game = manyOpenFrames(game, 10, 0, 0);
    expect(game.score(), 0);
    print(game.printedScore());
    expect(game.printedScore(), "-- -- -- -- -- -- -- -- -- --");
  });

  test('Threes', () {
    BowlingGame game = BowlingGame();

    game = manyOpenFrames(game, 10, 3, 3);
    expect(game.score(), 60);
    expect(game.printedScore(), "33 33 33 33 33 33 33 33 33 33");
  });

  test('Spare', () {
    BowlingGame game = BowlingGame();

    game.spare(4, 6);
    game.openFrame(3, 3);
    game = manyOpenFrames(game, 8, 0, 0);
    expect(game.score(), 19);
    expect(game.printedScore(), "4/ 33 -- -- -- -- -- -- -- --");
  });

  test('Strike', () {
    BowlingGame game = BowlingGame();

    game.strike();
    game.openFrame(5, 3);
    game = manyOpenFrames(game, 8, 0, 0);
    expect(game.score(), 26);
    expect(game.printedScore(), "X 53 -- -- -- -- -- -- -- --");
  });

  test('Strike Final Frame', () {
    BowlingGame game = BowlingGame();

    game = manyOpenFrames(game, 9, 0, 0);
    game.strike();
    game.bonusRoll(2);
    game.bonusRoll(3);
    expect(game.score(), 15);
    expect(game.printedScore(), "-- -- -- -- -- -- -- -- -- X 2 3");
  });

  test('Strike Perfect Game', () {
    BowlingGame game = BowlingGame();

    for (int i = 0; i < 10; i++) {
      game.strike();
    }
    game.bonusRoll(10);
    game.bonusRoll(10);
    expect(game.score(), 300);
    expect(game.printedScore(), "X X X X X X X X X X X X");
  });
}

BowlingGame manyOpenFrames(
    BowlingGame game, int count, int firstThrow, int secondThrow) {
  BowlingGame finalGame = game;

  for (int frameNumber = 0; frameNumber < count; frameNumber++) {
    finalGame.openFrame(firstThrow, secondThrow);
  }
  return finalGame;
}
